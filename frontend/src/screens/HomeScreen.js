import { Grid, Heading } from "@chakra-ui/react";

import ProductCard from "../components/ProductCard";
import products from "../products";

const HomeScreen = () => {
  return (
    <>
      <Heading
        as="h2"
        mb="8"
        fontSize="xl"
        color="#333333"
        textAlign="center"
        fontFamily="revert"
        fontWeight="bold"
      >
        Latest Products
      </Heading>

      <Grid templateColumns="1fr 1fr 1fr 1fr" gap="8">
        {Array.isArray(products) ? (
          products.map((prod) => <ProductCard key={prod._id} product={prod} />)
        ) : (
          <p>No products available</p>
        )}
      </Grid>
    </>
  );
};

export default HomeScreen;
