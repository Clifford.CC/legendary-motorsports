import { Box, Flex, Heading, Image, Link, Text } from "@chakra-ui/react";
import { Link as RouterLink } from "react-router-dom";

import Rating from "./Rating";

const ProductCard = ({ product }) => {
  return (
    <Link
      as={RouterLink}
      to={`/product/${product._id}`}
      _hover={{ textDecor: "none" }}
    >
      <Box borderRadius="lg" bgColor="whiteAlpha.600" _hover={{ shadow: "lg" }}>
        <Image
          src={product.image}
          alt={product.name}
          w="35rem"
          h="15rem"
          objectFit="cover"
        />
        <Flex py="5" px="4" direction="column" justifyContent="space-between">
          <Heading
            as="h4"
            fontSize="lg"
            mb="1"
            color="#333333"
            fontFamily="revert"
          >
            {product.name}
          </Heading>
          <Heading
            as="h4"
            fontSize="sm"
            mb="3"
            color="#333333"
            fontFamily="revert"
          >
            {product.brand}
          </Heading>

          <Flex alignItems="center" justifyContent="space-between">
            <Rating value={product.rating} color="#333333" />
            <Text fontSize="xl" fontWeight="bold" color="#333333">
              ${product.price}
            </Text>
          </Flex>
        </Flex>
      </Box>
    </Link>
  );
};

export default ProductCard;
