import { Box, Flex, Heading, Icon, Link } from "@chakra-ui/react";
import { useState } from "react";
import { HiMenuAlt3, HiShoppingBag, HiUser } from "react-icons/hi";
import { Link as RouterLink } from "react-router-dom";

import HeaderMenuItem from "./HeaderMenuItem";

const Header = () => {
  const [show, setShow] = useState(false);

  return (
    <Flex
      as="header"
      align="center"
      justifyContent="space-between"
      wrap="wrap"
      py="6"
      px="6"
      bgColor="#333333"
      w="100%"
      pos="fixed"
      top="0"
      left="0"
    >
      {/* Logo/Title */}
      <Link as={RouterLink} to="/">
        <Heading
          as="h1"
          color="#CD853F"
          fontWeight="bold"
          size="md"
          letterSpacing="wide"
          fontFamily="serif"
        >
          LM
        </Heading>
      </Link>

      {/* Hamburger Menu */}
      <Box onClick={() => setShow(!show)}>
        <Icon
          display={{ base: "block", md: "none" }}
          as={HiMenuAlt3}
          color="#CD853F"
          w="6"
          h="6"
        />
      </Box>

      {/* Menu */}
      <Box
        display={{ base: show ? "block" : "none", md: "flex" }}
        width={{ base: "full", md: "auto" }}
        mt={{ base: "4", md: "0" }}
      >
        <HeaderMenuItem url="/cart" label="Cart" icon={HiShoppingBag} />
        <HeaderMenuItem url="/login" label="Login" icon={HiUser} />
      </Box>
    </Flex>
  );
};

export default Header;
