import { Flex, Text } from "@chakra-ui/react";

const Footer = () => {
  return (
    <Flex as="footer" justifyContent="center" py="5" bgColor="#333333">
      <Text color="#CD853F">
        Copyright {new Date().getFullYear()}. RST Store. All rights reserved.
      </Text>
    </Flex>
  );
};

export default Footer;
